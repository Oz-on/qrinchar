import React, {Component} from 'react';
import {
    View,
    TouchableOpacity,
    Text,
    StyleSheet,
    Modal,
} from 'react-native';
import {ViroARSceneNavigator} from 'react-viro';
import MapboxGL from '@react-native-mapbox-gl/maps'
import HelloWorldSceneAR from "../../HelloWorldSceneAR";
import Data, {dist} from "../../websockets/communication";
import InitializationModal from "./InitializationModal";
import Geolocation from "react-native-geolocation-service";

MapboxGL.setAccessToken('pk.eyJ1Ijoib3NrYXJhZCIsImEiOiJjam50ZmtiMDIwcWc0M3BsZXo1dGNra21xIn0.I-fv11QomikhqrUfdqF0LQ');

const sharedProps = {
    apiKey: "API_KEY_HERE",
};

class HomeScreen extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isUserSet: false,
            isArOpened: false,
            sharedProps: sharedProps,
            players: [],
            gifts: [],
            isPresentCloseEnough: false,
            closestPresent: null,
            points: {
                santa: 0,
                qrinch: 0,
            },
        };
        this.getUserLocation = this.getUserLocation.bind(this);
        this.collectGift = this.collectGift.bind(this);
    }

    componentDidMount() {
        this._Data = new Data();
        setInterval(() => {
            this.setState({players: this._Data.ownPlayers});
            this.setState({gifts: this._Data.presents});
            this.setState({points: this._Data.points});
            this.getUserLocation().then((location) => {
                let currentLongitude = JSON.stringify(location.coords.longitude);
                let currentLatitude = JSON.stringify(location.coords.latitude);
                this._Data.sendCoords(currentLongitude, currentLatitude);

                if (!this.state.isPresentCloseEnough) {
                    this.state.gifts.forEach(present => {
                        const distance = dist(present, {
                            coord: {
                                latitude: currentLatitude,
                                longitude: currentLongitude
                            }
                        });

                        if (distance < 0.050) {
                            this.setState({
                                isPresentCloseEnough: true,
                                closestPresent: present,
                            })
                        }
                    })
                }
            });



        }, 3000);
    }

    getUserLocation(){
        return new Promise((resolve, reject) => {
            Geolocation.getCurrentPosition(
                location => resolve(location),
                error => reject(error),
            )
        })
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevState.isUserSet !== this.state.isUserSet && this.state.isUserSet) {
            this._Data.ownPlayers;
        }
    }

    collectGift() {
        this.setState({isArOpened: false});
        this._Data.pickUpPresent(this.state.closestPresent.id);
        this.setState({
            closestPresent: null,
            isPresentCloseEnough: null,
        })
    }

    render() {
        return(
        <View style={styles.container}>
            <Text>M: {this.state.points.santa}</Text>
            <Text>G: {this.state.points.qrinch}</Text>
            <InitializationModal
                isModalOpen={!this.state.isUserSet}
                registerUser={(username, isSanta) => {
                    this._Data.registerUser(username, isSanta);
                    setTimeout(() =>{
                        if(this._Data.id !== -1) {
                            this.setState({isUserSet: true});
                        }
                    }, 100)
                }}
            />
            <Modal
                animationType={"slide"}
                transparent={false}
                visible={this.state.isArOpened}
            >
                <TouchableOpacity
                    style={styles.arExitBtn}
                    onPress={() => this.setState({isArOpened: false})}
                >
                    <Text>X</Text>
                </TouchableOpacity>
                <ViroARSceneNavigator
                    {...this.state.sharedProps}
                    initialScene={{
                        scene: HelloWorldSceneAR,
                        passProps: {
                            giftClicked: () => this.collectGift(),
                            isPresentCloseEnough: this.state.isPresentCloseEnough
                        }
                    }}
                />
            </Modal>
            <MapboxGL.MapView
                logoEnabled={false}
                compassEnabled={false}
                rotateEnabled={false}
                style={styles.container}
            >
                <MapboxGL.UserLocation visible={true} />
                <MapboxGL.Camera
                    defaultSettings={{
                        zoomLevel: 20
                    }}
                    followUserLocation={true}
                    followUserMode={MapboxGL.UserTrackingModes.Follow}
                />
                <TouchableOpacity
                    style={styles.arBtn}
                    onPress={() => this.setState({isArOpened: true})} >
                    <Text style={styles.arBtnText}>Znajdź prezent</Text>
                </TouchableOpacity>
                {
                    this.state.players.map((player) => {
                        console.log(player);
                        if (player.coord.latitude === -1 && player.coord.longitude === -1) {
                            return;
                        }
                        return (<MapboxGL.PointAnnotation
                            id='pointAnnotation'
                            coordinate={[parseFloat(player.coord.longitude), parseFloat(player.coord.latitude)]}
                            selected={false}
                        >
                            <View style={styles.playersPoint} />
                        </MapboxGL.PointAnnotation>)
                    })
                }
                {
                    this.state.gifts.map((gift) => {
                        if (gift.coord.latitude === -1 && gift.coord.longitude === -1) {
                            return;
                        }
                        return (<MapboxGL.PointAnnotation
                            id='pointAnnotation'
                            coordinate={[parseFloat(gift.coord.longitude), parseFloat(gift.coord.latitude)]}
                            selected={false}
                        >
                            <View style={styles.playersPoint} />
                        </MapboxGL.PointAnnotation>)
                    })
                }
            </MapboxGL.MapView>
        </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    arBtn: {
        position: 'absolute',
        alignSelf: 'center',
        bottom: 10,
        backgroundColor: 'green',
        width: '50%',
        borderRadius: 20,
        padding: 10,
        alignItems: 'center',
    },
    arBtnText: {
        fontSize: 20,
        textAlign: 'center',
        color: '#fff'
    },
    playersPoint: {
        borderRadius: 50,
        backgroundColor: 'red',
        width: 10,
        height: 10,
    },
    arExitBtn: {
        borderRadius: 50,
        width: 40,
        height: 40,
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        zIndex: 100,
        backgroundColor: 'red',
        top: 20,
        right: 5
    },
});
export default HomeScreen;
